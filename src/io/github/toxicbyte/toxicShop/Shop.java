package io.github.toxicbyte.toxicShop;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Shop 
{
	private String name;
	private Inventory sell;
	private Inventory buy;
	private Inventory menu;
	private Cuboid loc;
	private int totalSell = 0;
	public Shop(String name, Cuboid loc, Inventory buy)
	{
		toxicShop ts = new toxicShop();
		@SuppressWarnings("deprecation")
		IconMenu menu = new IconMenu(ChatColor.YELLOW + "Menu", 9, new IconMenu.OptionClickEventHandler() {
            @Override
            public void onOptionClick(IconMenu.OptionClickEvent event) {
                if (event.getName().equalsIgnoreCase("Buy"))
                {
                	openBuy(event.getPlayer());
                }
                else if (event.getName().equalsIgnoreCase("Sell"))
                {
                	openSell(event.getPlayer());
                }
                event.setWillClose(true);
            }
        }, ts)
        .setOption(3, new ItemStack(Material.WOOL, 1, DyeColor.RED.getData()), "Buy", "Buy some new equipment.")
        .setOption(4, new ItemStack(Material.WOOL, 1, DyeColor.GREEN.getData()), "Sell", "Sell your items for some cash.");
		
		
		this.name = name;
		this.loc = loc;
		this.sell = Bukkit.createInventory(null, 54, ChatColor.RED + "Sell");
		this.buy = buy;
		this.menu = (Inventory) menu;
	}
	public Cuboid getArea()
	{
		return this.loc;
	}
	public Inventory getItems()
	{
		Inventory inv = null;
		return inv;
	}
	public void openSell(Player target)
	{
		target.openInventory(this.sell);
	}
	public void openBuy(Player target)
	{
		target.openInventory(this.buy);
	}
	public void openMenu(Player target)
	{
		target.openInventory(this.menu);
	}
	public void addBuy(ItemStack items, int value)
	{
		toxicShop ts = new toxicShop();
		if (ts.getConfig().get("Shops." + name + ".buy") != null)
		{
			List<String> buys = ts.getConfig().getStringList("Shops." + name + ".buy");
			buys.add(makeSave(items.getType().name(), String.valueOf(items.getAmount()), String.valueOf(value)));
		}
		else
		{   
		   List<String> buys = new ArrayList<String>();
		   buys.add(makeSave(items.getType().name(), String.valueOf(items.getAmount()), String.valueOf(value)));
		   ts.getConfig().set("Shops." + name + ".buy", buys);
		   ts.saveConfig();			
		}
	}
	public Inventory getBuy()
	{
		return this.buy;
	}
	public String getName()
	{
		return this.name;
	}
	private String makeSave(String mat, String amount, String value)
	{
		return mat + "-" + amount + "-" + value;
	}
	private String getMat(String word)
	{
		return word.split("-")[0];
	}
	private int getAmount(String word)
	{
		return Integer.parseInt(word.split("-")[1]);
	}
	private int getValue(String word)
	{
		return Integer.parseInt(word.split("-")[2]);
	}
	public void sellItem(ItemStack item, int price) 
	{	
		totalSell =+ price;
	}
	public int getTotalSell()
	{
		return totalSell;
	}
	public void payPlayer(int totalSellPrice, int totalItems) //totalItems is just there to specify total items sold in a message ._.
	{
		//Code for player to receive payment
		totalSell = 0;
	}
}
