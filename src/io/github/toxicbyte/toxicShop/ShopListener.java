package io.github.toxicbyte.toxicShop;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ShopListener implements Listener 
{
	private String prefix;
	private toxicShop plugin;
	private Shop shop;
	
	public ShopListener(toxicShop plugin)
	{
		this.plugin = plugin;
		prefix = plugin.getPrefix();
	}
	@EventHandler
	public void onMove(PlayerMoveEvent e)
	{
		if (plugin.getConfig().isConfigurationSection("Shops"))
		{
		for (String shops : plugin.getConfig().getConfigurationSection("Shops").getKeys(false))
		{
			if (e.getFrom
					().getBlockX() != e.getTo().getBlockX() 
					|| e.getFrom().getBlockY() != e.getTo().getBlockY() 
					|| e.getFrom().getBlockZ() != e.getTo().getBlockZ())
			{
				Cuboid c = new Cuboid(
						new Location(
						plugin.getServer().getWorld(plugin.getConfig().getString("Shops." + shops + ".coords" + ".world"))
						, plugin.getConfig().getInt("Shops." + shops + ".coords" + ".minX")
						, plugin.getConfig().getInt("Shops." + shops + ".coords" + ".minY")
						, plugin.getConfig().getInt("Shops." + shops + ".coords" + ".minZ")
						), 
						new Location(
						plugin.getServer().getWorld(plugin.getConfig().getString("Shops." + shops + ".coords" + ".world"))
						, plugin.getConfig().getInt("Shops." + shops + ".coords" + ".maxX")
						, plugin.getConfig().getInt("Shops." + shops + ".coords" + ".maxY")
						, plugin.getConfig().getInt("Shops." + shops + ".coords" + ".maxZ")));
				if (c.contains(e.getTo()))
				{
					if (plugin.timeOut.containsKey(e.getPlayer()) && plugin.timeOut.get(e.getPlayer()))
					{
						
					}
					else
					{
						if (plugin.getConfig().get("Shops." + shops + ".buy") != null)
						{
							IconMenu buy = new IconMenu("Buy", 9, new IconMenu.OptionClickEventHandler() {
				            @Override
				            public void onOptionClick(IconMenu.OptionClickEvent event) {
				            	//Add buying code here
				                event.setWillClose(true);
				            }
				        }, plugin);
							((Inventory) buy).setContents((ItemStack[]) plugin.getConfig().get("Shops." + shops + ".buy"));
							Shop shop = new Shop(shops, c, (Inventory) buy);
							plugin.inShop.put(e.getPlayer(), shop);
							shop.openMenu(e.getPlayer());
						}
						
					}
				}
				else
				{
					if (plugin.timeOut.containsKey(e.getPlayer()) && plugin.timeOut.get(e.getPlayer()))
					{
						plugin.timeOut.remove(e.getPlayer());
					}
				}
			}
		}
		}
	}
	
	@EventHandler
	public void onPlayerSelect(PlayerInteractEvent e)
	{
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK 
				&& e.getPlayer().hasPermission("toxicshop.select") 
				&& e.getPlayer().getItemInHand().getType() == Material.WOOD_SPADE)
		{
			plugin.setLoc2(e.getClickedBlock().getLocation());
			e.setCancelled(true);
			e.getPlayer().sendMessage(prefix + "Second location set!");
		}
		else if(e.getAction() == Action.LEFT_CLICK_BLOCK
				&& e.getPlayer().hasPermission("toxicshop.select") 
				&& e.getPlayer().getItemInHand().getType() == Material.WOOD_SPADE)
		{
			plugin.setLoc1(e.getClickedBlock().getLocation());
			e.setCancelled(true);
			e.getPlayer().sendMessage(prefix + "First location set!");
		}
	}
	
	@EventHandler
	public void onCloseInv(InventoryCloseEvent e)
	{
		if(plugin.inShop.containsKey(e.getPlayer()))
		{
			if(ChatColor.stripColor(e.getInventory().getName()) == "Sell")
			{	
				int i = 0;
				int totalItems = 0;
				while (e.getInventory().getItem(i) != null)
				{
					List<String> itemLore = e.getInventory().getItem(i).getItemMeta().getLore();
					for(String lore : itemLore)
					{
						  if(lore.contains("Value: "))
						  {
						    lore = lore.replaceAll("Value: ", "");
						    int price = Integer.valueOf(lore);
							shop.sellItem(e.getInventory().getItem(i), price);
						  }
					}
					totalItems++;
					i++;
				}
				int price = shop.getTotalSell();
				shop.payPlayer(price, totalItems);
				plugin.inShop.remove(e.getPlayer());
			}
		}
	}
}
