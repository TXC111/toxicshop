package io.github.toxicbyte.toxicShop;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_ts implements CommandExecutor 
{
	private toxicShop plugin;
	private Shop shop;
	private String prefix;
	public CMD_ts(toxicShop plugin)
	{
		this.plugin = plugin;
		prefix = plugin.getPrefix();
	}
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) 
	{
		if(cs instanceof Player && cmd.getName().equalsIgnoreCase("shop"))
		{
			final Player player = (Player) cs;
			if(args.length == 2)
			{
				if(args[0].equalsIgnoreCase("create"))
				{
					if(player.hasPermission("toxicshop.create"))
					{
						String shop = args[1];
						if (plugin.getLoc1() != null && plugin.getLoc2() != null)
						{
							Cuboid c = new Cuboid(plugin.getLoc1(), plugin.getLoc2());
							if (plugin.getConfig().getConfigurationSection("Shops").getKeys(false).contains(shop))
							{
								plugin.getConfig().set("Shops." + shop + ".coords" + ".minX", c.getLowerX());
								plugin.getConfig().set("Shops." + shop + ".coords" + ".minY", c.getLowerY());
								plugin.getConfig().set("Shops." + shop + ".coords" + ".minZ", c.getLowerZ());
								plugin.getConfig().set("Shops." + shop + ".coords" + ".maxX", c.getUpperX());
								plugin.getConfig().set("Shops." + shop + ".coords" + ".maxX", c.getUpperY());
								plugin.getConfig().set("Shops." + shop + ".coords" + ".maxX", c.getUpperZ());
								plugin.getConfig().set("Shops." + shop + ".coords" + ".world", c.getWorld().getName());
								plugin.getConfig().set("Shops." + shop + ".buy", null);
								plugin.getConfig().set("Shops." + shop + ".sell", null);
								plugin.saveConfig();
								player.sendMessage(prefix + "Shop created!");
							}
							else
							{
								plugin.getConfig().set("Shops." + shop + ".coords" + ".minX", c.getLowerX());
								plugin.getConfig().set("Shops." + shop + ".coords" + ".minY", c.getLowerY());
								plugin.getConfig().set("Shops." + shop + ".coords" + ".minZ", c.getLowerZ());
								plugin.getConfig().set("Shops." + shop + ".coords" + ".maxX", c.getUpperX());
								plugin.getConfig().set("Shops." + shop + ".coords" + ".maxX", c.getUpperY());
								plugin.getConfig().set("Shops." + shop + ".coords" + ".maxX", c.getUpperZ());
								plugin.getConfig().set("Shops." + shop + ".coords" + ".world", c.getWorld().getName());
								plugin.getConfig().set("Shops." + shop + ".buy", null);
								plugin.getConfig().set("Shops." + shop + ".sell", null);
								plugin.saveConfig();
								player.sendMessage(prefix + "Shop created!");
							}
						}
						else
						{
							player.sendMessage(prefix + "No regions selected!");
						}
					}
					else
					{
						player.sendMessage(prefix + "You don't have permission to perform this action!");
					}
				}
				else if(args[0].equalsIgnoreCase("delete"))
				{
					if(player.hasPermission("toxicshop.delete"))
					{
						String shop = args[1];
						if(plugin.getConfig().getConfigurationSection("Shops.").getKeys(false).contains(shop))
						{
							plugin.getConfig().set("Shops." + shop, null);
							plugin.saveConfig();
							player.sendMessage(prefix + "Shop deleted!");
						}
						else
						{
							player.sendMessage(prefix + "That shop doesn't exist!");
						}
					}
					else
					{
						player.sendMessage(prefix + "You don't have permission to perform this action!");
					}
				}
				else
				{
					player.sendMessage(prefix + "Correct usage: /shop help");
				}
			}
			else if(args.length == 1)
			{
				if (args[0].equalsIgnoreCase("help"))
				{
					
				}
				else
				{
					player.sendMessage("Correct usage: /shop help");
				}
			}
			else if(args.length == 3)
			{
				if(args[0].equalsIgnoreCase("addItem"))
				{
					if (player.hasPermission("toxicshop.additem"))
					{
						String shopName = args[1];
						if (plugin.getConfig().isConfigurationSection("Shops." + shopName))
						{
							List<String> itemLore = player.getItemInHand().getItemMeta().getLore();
							if (player.getItemInHand().hasItemMeta())
							{
								itemLore.add("Value: " + args[2]);
								shop.addBuy(player.getItemInHand(), Integer.valueOf(args[2]));
							}
							else
							{
								itemLore.add("Value: " + args[2]);
								shop.addBuy(player.getItemInHand(), Integer.valueOf(args[2]));
							}
							player.setItemInHand(null);
							player.sendMessage(prefix + ChatColor.GREEN + "Item set for sale!");
						}
						else
						{
							player.sendMessage(prefix + ChatColor.RED + "Shop doesn't exist!");
						}
					}
					else
					{
						player.sendMessage(prefix + ChatColor.RED + "You do not have the required permissions to perform this function.");
					}
				}
				else
				{
					player.sendMessage(prefix + ChatColor.RED + "Correct usage: /shop help");
				}
			}
			else
			{
				player.sendMessage("Correct usage: /shop help");
			}
		}
		return false;
	}

}
