package io.github.toxicbyte.toxicShop;

import java.util.HashMap;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class toxicShop extends JavaPlugin
{
	public HashMap<Player, Shop> inShop = new HashMap<Player, Shop>();
	public HashMap<Player, Boolean> timeOut = new HashMap<Player, Boolean>();
	private Location loc1 = null;
	private Location loc2 = null;
	private String prefix;
    private static final Logger log = Logger.getLogger("Minecraft");
    
    @Override
	public void onEnable()
	{
		this.saveDefaultConfig();
		this.getServer().getPluginManager().registerEvents(new ShopListener(this), this);
		//This causes errors still VVV
		//prefix = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("Prefix"));			
		this.getCommand("shop").setExecutor(new CMD_ts(this));
	}
	
	@Override
	public void onDisable()
	{
		log.info(String.format("[%s] Disabled Version %s", getDescription().getName(), getDescription().getVersion()));
	}
	public String getPrefix()
	{
		return prefix;
	}
	public void setLoc1(Location loc)
	{
		loc1 = loc;
	}
	public void setLoc2(Location loc)
	{
		loc2 = loc;
	}
	public Location getLoc1()
	{
		return loc1;
	}
	public Location getLoc2()
	{
		return loc2;
	}
}
